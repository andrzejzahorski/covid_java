Java: input (pliki csv), znacznik czasu (dane dt. czynnosci procesowej)

[24.05 15:38] Zbigniew Gontar
    
package myPackage;
import java.util.ArrayList;
import java.util.Locale;
import java.util.Scanner;
import java.io.*;


public class TScanner  implements Serializable {
    TScanner(String arg) throws IOException {
        ScannerIn = new Scanner(new FileReader(arg));
        ScannerIn.useDelimiter(";");
        ScannerIn.useLocale(Locale.US);
        this.daneJavaList = new ArrayList<TDaneJava>();
    }
    public void ScannerRead() throws IOException {
        while (ScannerIn.hasNextLine()){
            while (ScannerIn.hasNext())    {
                if (ScannerIn.hasNextDouble()) {
                    System.out.print(ScannerIn.nextDouble() + " "); 
                } else {        
                    System.out.print(ScannerIn.next() + " "); 
                }
            }
            System.out.println();
        }    
    }
    
//    public void ScannerReadData() throws IOException {
//        int i=1;
//        while (ScannerIn.hasNextLine()){
//            int num_fields = 0;
//            String line = ScannerIn.nextLine();
//            String fields[] = line.split(";");
//            num_fields = (i==1) ? fields.length : num_fields;
//            if(fields.length == num_fields) {
//                String name = fields[0];
//                String date = fields[1];
//                int step = Integer.valueOf(fields[2]);
//                double grade = Double.valueOf(fields[3]);
//                String lecturer = fields[4];
//                this.daneJavaList.add(new TDaneJava(name, date, step, grade, lecturer));    
//            }
//        }
//    }


    public void ScannerReadData() throws IOException {
        int i=1;
        while (ScannerIn.hasNextLine()){
            int num_fields = 0;
            String line = ScannerIn.nextLine();
            String fields[] = line.split(";");
            num_fields = (i==1) ? fields.length : num_fields;
            if(fields.length == num_fields) {
                int id = Integer.valueOf(fields[0]);
                String name = fields[1];
                String date = fields[2];
                int step = Integer.valueOf(fields[3]);
                double grade = Double.valueOf(fields[4]);
                String lecturer = fields[5];
                this.daneJavaList.add(new TDaneJava(id, name, date, step, grade, lecturer));    
            }
        }
    }    
    public void ScannerCloseOut() throws IOException{
        ScannerIn.close();
    }
    public ArrayList<TDaneJava> getData(){
        return this.daneJavaList;
    }
    
    Scanner ScannerIn;
    private ArrayList<TDaneJava> daneJavaList;
}
 




<https://teams.microsoft.com/l/message/19:0e402d092c644656bdd44538ad1282fe@thread.skype/1590327494455?tenantId=164e1b0e-c8e5-41a9-9bbb-6f7ed40eef04&amp;groupId=173014f0-5e88-459a-a61e-cf5c82798c48&amp;parentMessageId=1590326479300&amp;teamName=Java nd 1520 20192 237990-1536 C 100 Podstawy programowania w języku Java dr Zbigniew Gontar NMMS&amp;channelName=Ogólny&amp;createdTime=1590327494455>







------------------------------------------------------------------------------------------------


[24.05 15:45] Zbigniew Gontar
    
package myPackage;


public class TDaneJava {
    TDaneJava(int id, String activityName, String timeStamp, int activityWhen, 
            double activityGrade, String lecturerName){
        this.id =id;
        this.activityName = activityName;
        this.activityWhen = activityWhen;
        this.timeStamp = timeStamp;
        this.activityGrade = activityGrade;
        this.lecturerName = lecturerName;
        
    }
    public int getId(){
        return this.id;
    }
    public String getActivityName(){
        return this.activityName;
    }
    public int getActivityWhen(){
        return this.activityWhen;
    }
    public String gettimeStamp(){
        return this.timeStamp;
    }
    public double activityGrade(){
        return this.activityGrade;
    }
    public String lecturerName(){
        return this.lecturerName;
    }
    private int id;
    private String activityName;
    private int activityWhen;
    private String timeStamp;
    private double activityGrade;
    private String lecturerName;
}
 



<https://teams.microsoft.com/l/message/19:0e402d092c644656bdd44538ad1282fe@thread.skype/1590327957910?tenantId=164e1b0e-c8e5-41a9-9bbb-6f7ed40eef04&amp;groupId=173014f0-5e88-459a-a61e-cf5c82798c48&amp;parentMessageId=1590326479300&amp;teamName=Java nd 1520 20192 237990-1536 C 100 Podstawy programowania w języku Java dr Zbigniew Gontar NMMS&amp;channelName=Ogólny&amp;createdTime=1590327957910>