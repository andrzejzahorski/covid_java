import java.util.ArrayList;


public class TCzynnosc {
    TCzynnosc(String arg1){
        this.nazwaCzynnosci = arg1;
    }
    public void show(){
        System.out.print(" - " + this.nazwaCzynnosci + " ");
    }
    private String nazwaCzynnosci;
}


public class TEgzamin extends TCzynnosc {
    TEgzamin(String arg1, double arg2){
          super(arg1);
        this.ocenaEgzamin = arg2;
    }
    public void show(){
          super.show();
        System.out.println(" - " + this.ocenaEgzamin);
    }
    private double ocenaEgzamin;
}


public class TTV extends TCzynnosc{
      TTV(String arg1, String arg2){
          super(arg1);
          this.TVstation = arg2;
    }
    public void show(){
        super.show();
          System.out.println(" - " + this.TVstation);
    }  
      private String TVstation;
}


public class TProject extends TCzynnosc{
      TProject(String arg1){
          super(arg1);
          //...
    }
    public void show(){
        super.show();
          //...
    }  
      //private ...;
}



public class TProces {
    TProces(){
        listaCzynnosci = new ArrayList<TCzynnosc>();
        this.liczbaCzynnosci = 0;    
    }
    public void add(TCzynnosc arg){
        listaCzynnosci.add(arg);
        this.liczbaCzynnosci++;
    }
    public void show(){
        System.out.println("Oto lista " + liczbaCzynnosci +" czynnosci:");
        for (TCzynnosc tymczas : listaCzynnosci)
            tymczas.show();
    }
    public ArrayList<TCzynnosc> getListaCzynnosci(){
        return listaCzynnosci;
    }
    public int getLiczbaCzynnosci(){
        return liczbaCzynnosci;
    }
    private ArrayList<TCzynnosc> listaCzynnosci;
    private int liczbaCzynnosci;
}


public class TOgladalnosc extends TProces{
  TOgladalnosc(){
       super();    
  }
  public void add(TTV arg){
       super.add(arg);
  }
  public void show(){
    System.out.println("Oto lista " + super.getLiczbaCzynnosci() +" ogladanych programów:");
    for (TCzynnosc tymczas : super.getListaCzynnosci())
        ((TTV)tymczas).show();
  }
}


public class TSmartWarsaw extends TProces{
  TSmartWarsaw(){
       super();    
  }
  public void add(TProject arg){
       super.add(arg);
  }
  public void show(){
    System.out.println("Oto lista " + super.getLiczbaCzynnosci() +" projektów realizowanych w ramach programu SmartWarsaw:");
    for (TCzynnosc tymczas : super.getListaCzynnosci()){
        (tymczas).show();
          System.out.println();
    }
  }


}


public class TSesja extends TProces{
  TSesja(){
    super();
  }
  public void add(TEgzamin arg){
       super.add(arg);
  }
  public void show(){
    System.out.println("Oto lista " + super.getLiczbaCzynnosci() +" egzaminów w sesji:");
    for (TCzynnosc tymczas : super.getListaCzynnosci())
        ((TEgzamin)tymczas).show();
        
  }
}


public class TMain {
    public static void main(String []args){
      TEgzamin egzamin_1 = new TEgzamin("Programowanie w Java", 4.0);
      TEgzamin egzamin_2 = new TEgzamin("Big Data", 5.0);
      
      TProces proces_1 = new TProces();
      
      proces_1.add(egzamin_1);// egzamin_1 jest zdefiniowany w domenie TEgzamin, a argument funkcji add w domenie TCzynnosc
      proces_1.add(egzamin_2);
      proces_1.add(new TTV("Dom z papieru", "Netflix"));
      proces_1.show();
      
      TOgladalnosc ogladalnosc_1 = new TOgladalnosc();
      
      ogladalnosc_1.add(new TTV("Dom z papieru", "Netflix"));
      ogladalnosc_1.add(new TTV("Będzie dobrze, kochanie", "TVP2"));
      
      ogladalnosc_1.show();


      TSesja sesja_1 = new TSesja();
      
      sesja_1.add(new TEgzamin("Python", 3.0));
      sesja_1.add(new TEgzamin("SQL", 4.5));
      
      sesja_1.show();
      
      TSmartWarsaw mojObiekt = new TSmartWarsaw();
      //...
      mojObiekt.add(new TProject("Tramwaj wodny"));
      mojObiekt.add(new TProject("Autonomiczny autobus miejski"));
      mojObiekt.add(new TEgzamin("SQL", 4.5));
      mojObiekt.show();
      


    }
}














<https://teams.microsoft.com/l/message/19:0e402d092c644656bdd44538ad1282fe@thread.skype/1589122482449?tenantId=164e1b0e-c8e5-41a9-9bbb-6f7ed40eef04&amp;groupId=173014f0-5e88-459a-a61e-cf5c82798c48&amp;parentMessageId=1589116634363&amp;teamName=Java nd 1520 20192 237990-1536 C 100 Podstawy programowania w języku Java dr Zbigniew Gontar NMMS&amp;channelName=Ogólny&amp;createdTime=1589122482449>